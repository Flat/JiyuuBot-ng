JiyuuBot-ng
===========

.. .. image:: https://readthedocs.org/projects/jiyuubot/badge/?version=latest

Yet another IRC bot. Written in Vala. Requires Python 3.

JiyuuBot is a modular IRC bot based on libpurple, bringing pluggable
functionality to whatever chat platform one desires.

JiyuuBot core code and all modules are licensed under the AGPLv3.

.. See the docs at http://jiyuubot.readthedocs.org/en/latest/
